
import numpy as np
"""def build_array(x):
    a = [[0]*x for i in range(x)]
    for i in range(x):
        for j in range(x):
            if i == j:
                a[i][j] = 1
            else:
                continue
    x = np.array(a)
    print(x)
build_array(4)

def build_array_1(x):
    a = [[1]*x for i in range(x)]
    for i in range(x):
        for j in range(i+1, x):
            a[i][j] = 0
    for i in range(x):
        for j in range(0, i):
            a[i][j] = 0
    b = np.array(a)
    return b
print(build_array_1(4))"""


A = np.array([[2, 4, 6, 3]])

def my_reshape(massif, x, y):
    B = []
    rows = 0
    colums = 0
    B_index = 0
    # Переводим массив в одномерную плоскость
    for i in range(len(A)):
        rows += 1
        for j in range(len(A[i])):
            B.append(A[i][j])
            colums += 1
        colums /= rows
    C = [[0]*(y) for i in range((x))] # Создаем матрицу необходимого размера
    # Берем значения из одномерного массива и заталкиваем в созданную матрицу
    if ((x*y) % (rows*colums)) == 0:
        for i in range(x):
            for j in range(y):
                C[i][j] = B[B_index]
                B_index += 1
        C = np.array(C)
        return C
    else:
        print(np.array([]))

print(my_reshape(A, 2, 2))
