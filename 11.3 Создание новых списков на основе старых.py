
import numpy as np

income = np.array([
    [20000, 30000, 25000, 700000],
    [23000, 35000, 20000, 32000]
])

print(np.log(income))

L = [2, 4, 7, 1]
print(L)

L_sq = [i**2 for i in L]
print(L_sq)
