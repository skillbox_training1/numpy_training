
import numpy as np

scores = np.array([3, 5, 7, 9, 8, 10])
pairs = scores.reshape(3, 2)
print(pairs)

# Изменение значнеий в массиве
pairs[0][0] = 3
print(pairs)

pairs_2 = pairs
pairs_2[0][0] = 4
#Значения получились одинаковые хотя мы изменяли только pairs_2, но он является лишь ярлыком pairs, поэтому меняется все
print(pairs_2)
print(pairs)

# ЧТобы создать копию, которая не будет менять исходный массив, используем метод copy
pairs_3 = pairs_2.copy()
pairs_3[2][1] = 9
print(pairs_2)
print(pairs_3)

pairs[0:2] = [[5, 9], [9, 7]]
print(pairs)

