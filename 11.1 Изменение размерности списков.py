
import numpy as np

scores = np.array([3, 5, 7, 9, 8, 10])

# Преобразуем одномерный массив в двухмерный, т.е. из списка множества преобразовалив таблицу с 3 строками и 2 столбцами
pairs = scores.reshape(3, 2)
print(pairs)

# Чтобы посмтотреть из скольких мерносетей состоит массив
print(pairs.ndim)
print(scores.ndim)

# Преобразуем множество из 3*2 на 2*3
two_strok = pairs.transpose()
print(two_strok)

# Преобразование множества в одномерный "список"
t = two_strok.ravel()
print(t)
