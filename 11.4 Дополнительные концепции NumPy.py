import numpy as np

m = np.arange(1, 10, 0.003)
print(m)
print(m.round())
print(m.round(2))

A = np.array([2.5, 2.7, 8.1, 9.25])

# Данный метод "floor" позволяет округлять в меньшую сторону.
print(np.floor(A))

# Данный метод "ceil" позволяет округлять в большую сторону.
print(np.ceil(A))

# Метод "add" позволяет прибавить к массиву занчений необходимое значение
print(np.add(A, 2))

# Метод "subtract" позволяет вычитать значения в массиве
print(np.subtract(A, 1))

# Метод "sum" позволяет суммировать все значения в массиве.
print(np.sum(A))

# Метод "prod" умножает все занчения в массиве.
print(np.prod(A))

d1 = '2020-02-25'
d2 = '2020-02-28'

print(np.datetime64(d1))
print(np.datetime64(d2))
a = np.datetime64(d2) - np.datetime64(d1)
print(a)
